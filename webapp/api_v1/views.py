from django.shortcuts import get_list_or_404, get_object_or_404
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from api_v1.models import TradingTransactions, BalanceTransaction, Balance
from api_v1.serializers import BalanceTransactionSerializer, TradingTransactionSerializer, BalanceSerializer, UsersAndBalanceSerializer
from authentication.models import User


class BalanceTransactionView(APIView):
    def get(self, request, *args, **kwargs):
        objects = get_list_or_404(BalanceTransaction, user=request.user)
        serializer = BalanceTransactionSerializer(objects, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = BalanceTransactionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=400)


class TradingTransactionView(APIView):
    def get(self, request, *args, **kwargs):
        objects = get_list_or_404(TradingTransactions, user=request.user)
        serializer = TradingTransactionSerializer(objects, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        amount = request.data.get("amount")
        price = request.data.get("price")
        total = amount * price
        data = request.data
        data["total_price"] = total
        serializer = TradingTransactionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=400)


class ReplenishmentView(APIView):
    def post(self, request, *args, **kwargs):
        user_data = request.data.get("user")
        user_balance = get_object_or_404(Balance, user=user_data)
        balance = request.data.get("balance")
        serializer = BalanceSerializer(data=request.data)
        if serializer.is_valid():
            data = request.data
            data['balance'] = int(user_balance.balance) + int(balance)
            data['user'] = User.objects.get(pk=user_data)
            serializer.update(user_balance, validated_data=data)
            return Response({
                "balance": data['balance'],
                "user": data['user'].pk
            })
        else:
            return Response(serializer.errors, status=400)


class WithdrawalView(APIView):
    def post(self, request, *args, **kwargs):
        user_data = request.data.get("user")
        user_balance = get_object_or_404(Balance, user=user_data)
        balance = request.data.get("balance")
        serializer = BalanceSerializer(data=request.data)
        if serializer.is_valid():
            data = request.data
            if user_balance.balance >= balance:
                data['balance'] = int(user_balance.balance) - int(balance)
                data['user'] = User.objects.get(pk=user_data)
                serializer.update(user_balance, validated_data=data)
                return Response({
                    "balance": data['balance'],
                    "user": data['user'].pk
                })
            else:
                return Response({
                    "error": "Недостаточно средств на счете"
                })
        else:
            return Response(serializer.errors, status=400)


class UsersListView(APIView):
    permission_classes = (IsAdminUser,)

    def get(self, request, *args, **kwargs):
        objects = Balance.objects.all()
        serializer = UsersAndBalanceSerializer(objects, many=True)
        return Response(serializer.data)
