from django.contrib import admin

from api_v1.models import Balance, BalanceTransaction, Products, TradingTransactions


class BalanceAdmin(admin.ModelAdmin):
    list_display = ['id', 'balance', 'user']
    list_filter = ['user']
    search_fields = ['user']
    fields = ['balance', 'user']


class ProductAdmin(admin.ModelAdmin):
    list_display = ['id', 'product_name', 'amount', 'price', 'balance_id', 'buy_at', 'updated_at']
    list_filter = ['product_name']
    search_fields = ['product_name']
    fields = ['product_name', 'amount', 'price', 'balance_id', 'buy_at', 'updated_at']
    readonly_fields = ['buy_at', 'updated_at']


class BalanceTransactionAdmin(admin.ModelAdmin):
    list_display = ['transaction_number', 'type', 'price', 'user', 'datetime']
    list_filter = ['user']
    search_fields = ['user']
    fields = ['type', 'price', 'user', 'datetime']
    readonly_fields = ['datetime']


class TradingTransactionAdmin(admin.ModelAdmin):
    list_display = ['transaction_number', 'product', 'amount', 'type', 'price', 'total_price', 'user', 'datetime']
    list_filter = ['user']
    search_fields = ['user']
    fields = ['product', 'amount', 'type', 'price', 'total_price', 'user', 'datetime']
    readonly_fields = ['datetime']


admin.site.register(Balance, BalanceAdmin)
admin.site.register(Products, ProductAdmin)
admin.site.register(BalanceTransaction, BalanceTransactionAdmin)
admin.site.register(TradingTransactions, TradingTransactionAdmin)