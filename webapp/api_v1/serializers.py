from rest_framework import serializers
from api_v1.models import Balance, BalanceTransaction, TradingTransactions, Products
from authentication.models import User


class BalanceSerializer(serializers.ModelSerializer):
    """
        Сериализатор счета пользователя
    """
    class Meta:
        model = Balance
        fields = ['balance', 'user']


class ProductSerializer(serializers.ModelSerializer):
    """
        Сериализатор продуктов
    """
    class Meta:
        model = Products
        fields = ['product_name', 'balance_id', 'amount', 'price', 'buy_at', 'updated_at']
        read_only_fields = ['buy_at', 'updated_at']


class BalanceTransactionSerializer(serializers.ModelSerializer):
    """
        Сериализатор историй транзакции проведенных в отношении счета пользователя
    """
    class Meta:
        model = BalanceTransaction
        fields = ['transaction_number', 'price', 'type', 'user', 'datetime']
        # Дата и время добавляются автоматически, поэтому укажем его как только для чтения
        read_only_fields = ['datetime']


class TradingTransactionSerializer(serializers.ModelSerializer):
    """
        Сериализатор историй транзакции проведенных по купле-продаже товаров
    """
    class Meta:
        model = TradingTransactions
        fields = ['transaction_number', 'product', 'amount', 'price', 'total_price', 'type', 'user', 'datetime']
        read_only_fields = ['datetime']


class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email']


class UsersAndBalanceSerializer(serializers.ModelSerializer):
    """
        Сериализатор юзера и баланса
    """
    user = UsersSerializer(many=False, read_only=True)

    class Meta:
        model = Balance
        fields = ['id', 'balance', 'user']
