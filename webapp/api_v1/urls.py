from django.urls import path
from api_v1.views import BalanceTransactionView, TradingTransactionView, WithdrawalView, ReplenishmentView, \
    UsersListView

urlpatterns = [
    path('balance_transaction/', BalanceTransactionView.as_view(), name="balance_transaction_view"),
    path('trading_transaction/', TradingTransactionView.as_view(), name="trading_transaction_view"),
    path('replenishment/', ReplenishmentView.as_view(), name="replenishment_view"),
    path('withdrawal/', WithdrawalView.as_view(), name="withdrawal_view"),
    path('users_list/', UsersListView.as_view(), name="users_list")
]
