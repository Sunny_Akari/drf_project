import uuid

from django.contrib.auth import get_user_model
from django.db import models


BALANCE_TRANSACTION_TYPE = (
    ('REPLENISHMENT', 'replenishment'),
    ('WITHDRAWAL', 'withdrawal')
)

TRADING_TRANSACTION_TYPE = (
    ('BUY', 'buy'),
    ('SELL', 'sell')
)


class Balance(models.Model):
    """
       Создаем модель счета пользователя
    """
    balance = models.PositiveIntegerField()
    user = models.ForeignKey(get_user_model(),  on_delete=models.CASCADE, related_name='balance_user')

    def __str__(self):
        return f"{self.id}"


class Products(models.Model):
    """
        Создаем модель для торговли продуктами, каждый продукт прикреплен к определенному счету
    """
    product_name = models.CharField(max_length=200, null=False, blank=False)
    amount = models.PositiveIntegerField(null=False, blank=False)
    price = models.PositiveIntegerField(null=False, blank=False)
    balance_id = models.ForeignKey('api_v1.Balance', on_delete=models.CASCADE, related_name='user_balance')
    buy_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.product_name}"


class BalanceTransaction(models.Model):
    """
        Модель истории транзакции связанные с пополнением и снятием со счета
    """
    transaction_number = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    datetime = models.DateTimeField(auto_now_add=True)
    price = models.PositiveIntegerField(null=False, blank=False)
    type = models.CharField(max_length=100, choices=BALANCE_TRANSACTION_TYPE, default='REPLENISHMENT')
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='balance_transaction_user')

    def __str__(self):
        return f"{self.transaction_number}"


class TradingTransactions(models.Model):
    """
        Модель историй транзакции связанные с куплей-продажей товаров
    """
    transaction_number = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    datetime = models.DateTimeField(auto_now_add=True)
    product = models.ForeignKey('api_v1.Products', on_delete=models.CASCADE, related_name='trading_product')
    amount = models.PositiveIntegerField(null=False, blank=False)
    price = models.PositiveIntegerField(null=False, blank=False)
    total_price = models.PositiveIntegerField(null=False, blank=False)
    type = models.CharField(max_length=100, choices=TRADING_TRANSACTION_TYPE, default='BUY')
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='trading_transaction_user')

    def __str__(self):
        return f"{self.transaction_number}"

