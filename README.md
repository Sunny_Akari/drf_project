## drf_project

URL и примеры входных данных для проверки проекта на постмане

## Создание нового пользователя (Регистрация)
- method: POST
- URL: http://localhost:8000/authentication/users/

```
{
    "user": {
        "username": "user1",
        "email": "user1@user.user",
        "password": "user1111"
    }
}

```

## LOGIN
- method: POST
- URL: http://localhost:8000/authentication/users/login/
```
{
    "user": {
        "email": "user1@user.user",
        "password": "user1111"
    }
}

```

## Аутентификация
Добавить в заголовок постмана

```
Headers Token asfdg43t4532523532532532fsdfdsfsdfdsfdsfsdf
```

## Запросы по транзакциям

# Добавление в историю транзакции снятие и добавление на счет
- method: POST
- URL: http://localhost:8000/api_v1/balance_transaction/
```
{
    "user": 1,
    "type":"REPLENISHMENT/WITHDRAWAL",
    "price":200
}


```
# Получение списка всех транзакции пользователя
- method: GET
- URL: http://localhost:8000/api_v1/balance_transaction/


# Добавление в историю транзакции записи о покупке и продаже
- method: POST
- URL: http://localhost:8000/api_v1/trading_transaction/
```
{
    "user": 1,
    "type":"BUY/SELL",
    "product":1,
    "amount":1,
    "price":200
}

```
# Получение списка всех транзакции пользователя
- method: GET
- http://localhost:8000/api_v1/trading_transaction/


# Получение списка всех пользователей (только для админов)

- method: GET
- URL: http://localhost:8000/api_v1/users_list/


